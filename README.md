# Ecocup - Reducing Single Use Plastics	<br>
| For Companies |  | For Users |
| ------ | ------ | ------ |
| <img src="https://429860629.linuxzone125.grserver.gr/ecocup-api//user/images/stores-gif.gif" width="200" height="auto"/> | <img src="https://429860629.linuxzone125.grserver.gr/ecocup-api//user/images/ecocup.png" width="200" height="auto"/> | <img src="https://429860629.linuxzone125.grserver.gr/ecocup-api//user/images/user-gif.gif" width="200" height="444"/> |



## Description <br>
**Ecocup** is a **cross-platform** mobile application targeted to help overcome the escalating **problem of single use plastics**, which is leading towards an environmental disaster. Millions of animals are killed by plastics every year from birds to fish, and other marine organisms. Nearly 700 species, including endagered ones, are known to have been affected by plastics - almost every species of seabirds eat plastics. <br>
<br>
Plastics that are **properly captured** and disposed through municipal solid waste systems pose **minimal risk** to ecosystems;
however, plastics that enter ecosystems through **mismanagement and “leakage”** can pose **physical and chemical hazards** to
wildlife.<br>
<br>
Major efforts are taking place across the globe to address our singleuse plastics problem and resulting ecological damage, many of them including governments reducing taxes on companies with low environmental footprint, huge campaigns taking place to raise public awareness and state-of-the-art technology being created in order to reduce ocean plastics.<br>
<br>
With **Ecocup** we are providing a digital solution that will help **companies** decisevely reduce their **environmental footprint**, by producing less plastic waste as well as **people** receive **benefits and rewards** by using their re-usable (eco)cup.

## How it works
Business owners have a unique QR code which is generated from the application. Users that use Ecocup application, can find which stores are **Ecocup compatible** through the stores map that is provided within the application. Users scan the store's QR code, and the transaction is completed. Based on every store's reward system, users will receive rewards, discounts and gifts according to how many times their cup has been used.<br>
Furthermore, we keep statistics regarding the environmental contributions of each user and store ( e.g. trees saved, water saved etc ) which are resulting to tax discounts for stores, rewards on certain checkpoints for users and other fun operations.

<img src="https://429860629.linuxzone125.grserver.gr/ecocup-api//user/images/ecocup-scan.gif" width="auto" height="auto"/>

## Roadmap
<img src="https://429860629.linuxzone125.grserver.gr/ecocup-api//user/images/ecocup-roadmap.png" width="auto" height="auto"/>
