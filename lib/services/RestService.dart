import 'package:http/http.dart' as http;

class RestService{

  Future<String> doPostRequest(String url, Map<String,String> body) async{
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.fields.addAll(body);
    http.StreamedResponse response = await request.send();
    String authResponse = await response.stream.bytesToString();
    return authResponse;
  }

}