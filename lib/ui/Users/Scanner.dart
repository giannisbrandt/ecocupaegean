import 'dart:async';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/ui/Dialogs/ScanFailure.dart';
import 'package:Ecocup/ui/Dialogs/ScanSuccess.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ChooseLoginPage.dart';

class ScanScreen extends StatefulWidget {
  @override
  _ScanState createState() => new _ScanState();
}

class _ScanState extends State<ScanScreen> {
  String barcode = "";
  int user_id = -1;
  String name = "";
  String access_token = "";
  String updateResponse = "";

  @override
  void initState(){
    super.initState();
    _loadCommons();
  }

  Future<String> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
              color: Colors.white
          ),
          title: const Text('Ecocup', style: TextStyle(
              color: Colors.white
          )),
          backgroundColor: Colors.blue,
        ),
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.only(left: 0,right: 0,top: 0,bottom: 15),
                  width: 370,
                  height: 370,
                  child: Align(
                    alignment: Alignment.center,
                    child: Image.asset('assets/images/scanandearn.png'),
                  )
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 18.0),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: RaisedButton(
                    onPressed: (){
                      scan();
                    },
                    color: Colors.blueGrey,
                    shape:  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0),
                    ),
                    child: Container(
                      color: Colors.blueGrey,
                      constraints: BoxConstraints(maxWidth: 190.0,maxHeight: 40.0,),
                      alignment: Alignment.center,
                      child: Text(
                        "Scan",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            letterSpacing: 2.0,
                            fontWeight: FontWeight.w300
                        ),
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ));
  }

  Future<String> updateContributionsTransactions() async{
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
      'barcode': barcode,
    });

    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String userContributionsRoute = await RouteService().getRoute(this.context,"UpdateUserContributions");
    String response = await RestService().doPostRequest(baseUri+userContributionsRoute, body);

    updateResponse = response;
    return response;
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() {
        this.barcode = barcode;
        //REST call to update transactions and contributions.
      });
      await updateContributionsTransactions().then((value) =>
          showDialog(
          context: context,
          builder: (BuildContext context) {
            if(!updateResponse.contains("status")){
              return ScanSuccessDialogBox(
                title: updateResponse,
                descriptions: "Your transaction was successful",
                text: "Done",
              );
            }
            else {
              return ScanFailureDialogBox(
                title: "Transaction Failed",
                descriptions: "Please scan a valid QR Code",
                text: "Done",
              );
            }

          })
      );

    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'Camera permission is needed';
        });
      } else {
        setState(() => this.barcode = 'An error occured.Please try again.');
      }
    } on FormatException{
      setState(() => this.barcode = 'Not Found');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error');
    }
  }
}