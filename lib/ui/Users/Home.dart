import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class Home extends StatefulWidget{
  Home({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int user_id = 0;
  String name = "";
  String access_token = "";
  String contribWater="0",contribTrees="0",contribCo2="0",contribMoNH="0";
  String contribSingleUseCupsSaved="0";
  String contributionsResponse = "";
  String messageTitle = "Empty";
  String notificationAlert = "alert";
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool commonsLoaded = false;

  @override
  void initState(){
    super.initState();
    if(!commonsLoaded) {
      _loadCommons();
      commonsLoaded = false;
    }
  }

  Future<void> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      registerNotification();
      _firebaseMessaging.configure(
        onMessage: (message) async{
          setState(() {
            messageTitle = message["notification"]["title"];
            notificationAlert = "New Notification Alert";
          });

        },
        onResume: (message) async{
          setState(() {
            messageTitle = message["data"]["title"];
            notificationAlert = "Application opened from Notification";
          });

        },
      );
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  void registerNotification() async {
    _firebaseMessaging.getToken().then((token) {
      print('Token: $token');
    }).catchError((e) {
      print(e);
    });
  }

  Future<String> getUserContributions() async{
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });

    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String userContributionsRoute = await RouteService().getRoute(this.context,"UserContributionsRoute");
    contributionsResponse = await RestService().doPostRequest(baseUri+userContributionsRoute, body);

      try{
        Map<String,dynamic> parsedAContributionsResponse = jsonDecode(contributionsResponse);
        contribSingleUseCupsSaved = parsedAContributionsResponse['sucs'];
        contribWater = (parsedAContributionsResponse['water'] ?? 0);
        contribTrees = (parsedAContributionsResponse['trees'] ?? 0);
        contribCo2 = (parsedAContributionsResponse['co2'] ?? 0);
        contribMoNH = (parsedAContributionsResponse['monh'] ?? 0);
      }
      catch(e){
        contribSingleUseCupsSaved = "0";
        contribWater = "0.00";
        contribTrees = "0.00";
        contribCo2 = "0.00";
        contribMoNH = "0.00";
      }
    return contributionsResponse;
  }

  Widget homeWidget(BuildContext context) {
    return FutureBuilder(
      future: getUserContributions(),
      builder: (context, snapshot) {
        if (snapshot.hasData || snapshot.connectionState == ConnectionState.done) {
          return
            SafeArea(
            child: SingleChildScrollView (
             child:Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        decoration: new BoxDecoration(
                          boxShadow: [
                            new BoxShadow(
                              color: Colors.white38,
                              blurRadius: 10.0,
                            ),
                          ],
                        ),
                        child: Card(
                          child: Column(children: [
                            Container(
                              padding: EdgeInsets.all(10),
                              width: MediaQuery.of(context).size.width - 30,
                              height: 90,
                              child: Image.asset('assets/images/cup.png'),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Text("Single use cups saved",
                                style: TextStyle(
                                    fontSize: 18.0,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600)),
                            SizedBox(
                              height: 7,
                            ),
                            Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  contribSingleUseCupsSaved,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w300),
                                )),
                          ]),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Your environmental contributions"
                      ,style: TextStyle(
                        fontSize: 15.0,
                        color:Colors.black,
                        fontWeight: FontWeight.bold
                    ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: new BoxDecoration(
                        boxShadow: [
                          new BoxShadow(
                            color: Colors.white38,
                            blurRadius: 20.0,
                          ),
                        ],
                      ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: Card(
                            child:
                              Column(
                                children: [
                                  Container(
                                    width: 50,
                                    height: 90,
                                    child: Image.asset('assets/images/waterdrops.png'),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text("Water",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w600
                                    ),),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Text(
                                      contribWater + " Lt",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w300
                                      ),),
                                  ),

                                ]
                              ),
                          ),
                        ),
                        Expanded(
                          child:
                          Card(
                            child: Column(children:[
                              Container(
                                width: 50,
                                height: 90,
                                child: Image.asset('assets/images/tree.png'),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Trees",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w600
                                ),),
                              SizedBox(
                                height: 7,
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  contribTrees + " trees",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w300
                                  ),)
                              ),
                            ]),
                          ),
                        ),
                      ],
                    ),
                  )
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                          child: Card(
                            child: Column( children: [
                              Container(
                                width: 50,
                                height: 90,
                                child: Image.asset('assets/images/co2.png'),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "CO2",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w600
                                ),),
                              SizedBox(
                                height: 7,
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child:  Text(
                                    contribCo2 + " kg",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w300
                                    ),)
                              ),
                            ],
                          ),),
                        ),
                        Expanded(
                          child:
                          Card(
                            child: Column(children: [
                              Container(
                                width: 50,
                                height: 90,
                                child: Image.asset('assets/images/planetearth.png'),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Natural habitat",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w600
                                ),),
                              SizedBox(
                                height: 7,
                              ),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: Text(
                                  contribMoNH + " m\u00b2",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w300
                                  ),)
                              ),
                            ],
                          ),
                        ),
                        ),
                      ],
                    ),
                  ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          )
          );
        }
        else {//if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return homeWidget(context);
  }
}

