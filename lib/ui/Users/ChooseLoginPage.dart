
import 'package:flutter/material.dart';
import '../Business/Login.dart';
import 'Login.dart';

class ChooseLoginPage extends StatefulWidget {
  ChooseLoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ChooseLoginPageState createState() => _ChooseLoginPageState();
}

class _ChooseLoginPageState extends State<ChooseLoginPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        title: const Text('Ecocup', style: TextStyle(
            color: Colors.black
        )),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: Container(
                      width: 200,
                      height: 150,
                      child: Image.asset('assets/images/ecocup.png')),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.lightBlueAccent, borderRadius: BorderRadius.circular(20)),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => LoginPage()));
                  },
                  child: Text(
                    'I am a user',
                    style: TextStyle(color: Colors.white, fontSize: 19),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.orange,
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => BusinessLoginPage()));
                  },
                  child: Text(
                    'I am a business owner',
                    style: TextStyle(color: Colors.white, fontSize: 19),
                  ),
                ),
              ),
              SizedBox(
                height: 150,
              ),
              Text(
                'The power is in your cup.',
                style: TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
    );
  }
}