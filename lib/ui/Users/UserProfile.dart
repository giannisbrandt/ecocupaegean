import 'package:Ecocup/models/Friend.dart';
import 'package:Ecocup/models/VisitedStore.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../Dialogs/FriendDialogBox.dart';
import 'Stores.dart';


class UserProfile extends StatefulWidget{
  UserProfile({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  String storesResponse = "";
  String messageTitle = "Empty";
  String notificationAlert = "alert";
  List<Friend> _friends = [];
  List<VisitedStore> _visitedStores = [];

  @override
  void initState(){
    super.initState();
    _loadCommons();
    _loadFriends();
  }

  Future<void> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  Future<List<Friend>> _loadFriends() async {
    baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String friendsRoute = await RouteService().getRoute(this.context,"GetUserFriends");
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });
    String friendsResponse = await RestService().doPostRequest(baseUri+friendsRoute, body);
    var list = json.decode(friendsResponse) as List;
    _friends = list.map((i)=>Friend.fromJson(i)).toList();

    return Future.value(_friends);
  }

  Future<String> getUserVisitedStores() async{
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });

    baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String allStoresRoute = await RouteService().getRoute(this.context,"GetVisitedStores");
    storesResponse = await RestService().doPostRequest(baseUri+allStoresRoute, body);
    setState(() {
      var list = json.decode(storesResponse) as List;
      _visitedStores = list.map((i)=>VisitedStore.fromJson(i)).toList();
    });
    return Future.value(storesResponse);
  }

  Widget userDetailsWidget(BuildContext context) {
    return FutureBuilder(
      future: getUserVisitedStores(),
      // ignore: missing_return
      builder: (context, snapshot) {
        if (snapshot.hasData ||
            snapshot.connectionState == ConnectionState.done) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Container(
                            width: 150,
                            height: 150,
                            padding: EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 50, bottom: 0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(18.0),
                              child: Image.asset('assets/images/ecocup.png'),
                            ),
                          ),
                          SizedBox(
                            height: 40,
                          ),
                          Text(
                            name,
                            style: TextStyle(
                                fontSize: 25.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 25.00),
                    child: _tabSection(context),
                  ),
                ],
              ),
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _tabSection(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child:  TabBar(
              indicatorSize: TabBarIndicatorSize.tab,
              indicator: new BubbleTabIndicator(
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
                indicatorHeight: 40.0,
                indicatorColor: Colors.lightBlue,
              ),
              labelStyle: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600, color: Colors.black),
              labelColor: Colors.white,
              unselectedLabelColor: Colors.black,
              tabs: <Widget>[
                Text('Stores'),
                Text('Friends'),
              ],
              onTap: (index) {},
            )
          ),
          Container(
            //Add this to give height
            height: MediaQuery.of(context).size.height,
            child: TabBarView(children: [
              Container(
                margin: new EdgeInsets.only(top: 10.00),
                child: new ListView.builder(
                  itemCount: _visitedStores.length,
                  itemBuilder: _buildStoresListTile,
                ),
              ),
              Container(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 25.00,
                      ),
                  ElevatedButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                              return FriendDialogBox(
                                title: "Add a friend",
                                descriptions: "Your transaction was successful",
                                text: "Done",
                              );
                          });
                    },
                    child: Text(
                      'Add Friend',
                      style: TextStyle(color: Colors.blue, fontSize: 18),
                    ),
                    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                            side: BorderSide(color: Colors.blue)
                        )
                    )),
                  ),
                  Expanded(
                    child: new ListView.builder(
                      itemCount: _friends.length,
                      itemBuilder: _buildFriendListTile,
                    ),
                  )
                ],
              )),
            ]),
          ),
        ],
      ),
    );
  }

  Widget _buildFriendListTile(BuildContext context, int index) {
    var friend = _friends[index];

    return new ListTile(
      // onTap: () => _navigateToFriendDetails(friend, index),
      leading: new Hero(
          tag: index,
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Image.network(
                'https://429860629.linuxzone125.grserver.gr/ecocup-api/user/images/leaf.png',
                fit: BoxFit.cover,
              ),
            ),
          )),
      title: new Text(friend.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
      subtitle: new Text(friend.email),
    );
  }

  // void _navigateToFriendDetails(Friend friend, Object avatarTag) {
  //   Navigator.of(context).push(
  //     new MaterialPageRoute(
  //       builder: (c) {
  //         //return new FriendDetailsPage(friend, avatarTag: avatarTag);
  //       },
  //     ),
  //   );
  // }

  Widget _buildStoresListTile(BuildContext context, int index) {
    var store = _visitedStores[index];

    return new ListTile(
      //onTap: () => _navigateToFriendDetails(store, index),
      leading: new Hero(
        tag: index,
        child: new CircleAvatar(
          backgroundImage: new NetworkImage("https://429860629.linuxzone125.grserver.gr/ecocup-api/user/images/ecocup.png"),
        ),
      ),
      title: new Text(store.name,  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
      subtitle: new Text("Last visited on: " + store.lastSeen),
    );
  }

  @override
  Widget build(BuildContext context) {
    return userDetailsWidget(context);
  }
}