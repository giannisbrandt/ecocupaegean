import 'dart:async';
import 'dart:convert';
import 'package:Ecocup/main.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/services/ValidatorService.dart';
import 'package:Ecocup/ui/Business/main.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'Signup.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BusinessLoginPage extends StatefulWidget {
  BusinessLoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _BusinessLoginPageState createState() => _BusinessLoginPageState();
}

class _BusinessLoginPageState extends State<BusinessLoginPage> {
  final _formKey = GlobalKey<FormState>();

  String email = "";
  String password = "";

  String credentialsResponse = "";
  String authResponse = "";

  String name = "";
  String access_token = "";
  String user_id = "";

  bool isLoading = false;

  Future<String> credentials() async{
    Map<String,String> body = new Map();
    body.addAll({
      'email':email,
      'password':password
    });

    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String userInfoRoute = await RouteService().getRoute(this.context,"BusinessInfoRoute");
    credentialsResponse = await RestService().doPostRequest(baseUri+userInfoRoute, body);
    return credentialsResponse;
  }

  Future<String> authenticate(String user_credentials) async{
    Map<String,String> body = new Map();
    try{
      Map<String,dynamic> parsedACredentialsResponse;
      if(user_credentials != false) {
        parsedACredentialsResponse = jsonDecode(user_credentials);
        body.addAll({
          'access_token': parsedACredentialsResponse['code'],
          'store_id': parsedACredentialsResponse['id'],
          'email': parsedACredentialsResponse['email'],
          'password': password
        });

        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.clear();
        prefs.setInt('store_id', int.parse(parsedACredentialsResponse['id']));
        prefs.setString('name', parsedACredentialsResponse['name']);
        prefs.setString('access_token', parsedACredentialsResponse['code']);
        prefs.setBool('isFirstTimeBusiness', false);

        String baseUri = await RouteService().getRoute(this.context, "BaseUri");
        String loginRoute = await RouteService().getRoute(
            this.context, "BusinessLoginRoute");
        authResponse = await RestService().doPostRequest(baseUri + loginRoute, body);
      }
    }
    catch(e){}

    return authResponse;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Ecocup', style: TextStyle(
            color: Colors.black
        )),
        backgroundColor: Colors.white,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child:Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: Container(
                      width: 200,
                      height: 150,
                      child: Image.asset('assets/images/ecocup-orange.png')),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left:15.0,right: 15.0,top:50,bottom: 0),
                child: TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),

                        ),
                        labelText: 'Email',
                        labelStyle: new TextStyle(color: Colors.orange),
                        focusColor: Colors.orange,
                        hoverColor: Colors.orange,
                        hintText: 'yourname@gmail.com'),
                    validator: (value){
                      bool isValid = ValidatorService().validateEmail(value);
                      if(!isValid) return 'Please enter a valid email';
                      else email = value;
                      return null;
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),

                        ),
                        labelText: 'Password',
                        labelStyle: new TextStyle(color: Colors.orange),
                        hintText: 'Enter your password'),
                    validator: (value) {
                      //bool isValid = ValidatorService().validatePassword(value);
                      bool isValid = true;
                      if (!isValid) {
                        return 'Password must be at least 6 characters long';
                      }
                      else{
                        password = value;
                      }
                      return null;
                    }
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 145, bottom: 0),
                  child: FlatButton(
                    onPressed: (){
                      //TODO FORGOT PASSWORD
                    },
                    child: Text(
                      'Forgot Password',
                      style: TextStyle(color: Colors.orange, fontSize: 15),
                    ),
                  )
              ),
              isLoading ? CircularProgressIndicator():Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.orange, borderRadius: BorderRadius.circular(20)),
                child: FlatButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      setState(() {
                        isLoading = true;
                      });
                      try{
                        credentials().then((value) {
                          credentialsResponse = value;
                          authenticate(credentialsResponse).then((value) {
                            authResponse = value;
                            if (authResponse != "" && authResponse != " " && authResponse != null) {
                              Map<String, dynamic> parsedAuthResponse = jsonDecode(authResponse);

                              if (parsedAuthResponse['status'] != "") {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (_) =>
                                        BusinessHomePage(title: 'Ecocup')));
                              }
                            }
                            else{
                              Fluttertoast.showToast(
                                  msg: "Invalid username or password",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.red,
                                  textColor: Colors.white,
                                  fontSize: 16.0
                              );
                            }
                          });
                        });
                      }
                      catch (Exception ){
                        setState(() {
                          isLoading = false;
                        });
                      }
                      setState(() {
                        isLoading = false;
                      });
                    }
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: new GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => BusinessSignUpPage(title: 'Ecocup')));
                  },
                  child: new Text("Don't have an account? Register"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}