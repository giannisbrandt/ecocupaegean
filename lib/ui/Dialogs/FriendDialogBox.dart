import 'dart:convert';
import 'dart:ui';
import 'package:Ecocup/models/Friend.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'ScanSuccess.dart';

class FriendDialogBox extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;

  const FriendDialogBox({Key key, this.title, this.descriptions, this.text, this.img}) : super(key: key);

  @override
  _FriendDialogBoxState createState() => _FriendDialogBoxState();
}

class _FriendDialogBoxState extends State<FriendDialogBox> {

  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  List<Friend> _users = [];

  @override
  void initState(){
    super.initState();
    _loadCommons();
  }

  Future<void> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  Future<String> _addFriend(String asked_user_id) async {
    baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String addFriendRoute = await RouteService().getRoute(this.context,"AddFriend");
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
      'asked_user_id': asked_user_id,
      'name': name
    });
    String addFriendResponse = await RestService().doPostRequest(baseUri+addFriendRoute, body);
    return addFriendResponse;
  }

  Future<List<Friend>> _loadAllUsers() async {
    baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String allUsersRoute = await RouteService().getRoute(this.context,"GetAllUsers");
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });
    String allUsersResponse = await RestService().doPostRequest(baseUri+allUsersRoute, body);
    var list = json.decode(allUsersResponse) as List;
    _users = list.map((i)=>Friend.fromJson(i)).toList();
    return Future.value(_users);
  }

  Widget _buildUsersListTile(BuildContext context, int index) {
    var friend = _users[index];
    return new ListTile(
      // onTap: () => _navigateToFriendDetails(friend, index),
      leading: new Hero(
        tag: index,
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Image.network(
                'https://429860629.linuxzone125.grserver.gr/ecocup-api/user/images/leaf.png',
                fit: BoxFit.cover,
              ),
            ),
          )
      ),
      title: new Text(friend.name),
      subtitle: new Text(friend.email),
      trailing: IconButton(
        onPressed: (){
          _addFriend(friend.id);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return ScanSuccessDialogBox(
                  title: "Friend request sent",
                  descriptions: "The user will get notified for your friend request",
                  text: "Done",
                );
              });
        },
        icon: new Icon(Icons.add_circle_outline),
        color: Colors.lightBlueAccent,
        iconSize: 30.0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return FutureBuilder(
        future: _loadAllUsers(),
        builder: (context, snapshot) {
          if (snapshot.hasData ||
              snapshot.connectionState == ConnectionState.done) {
            return Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 70),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black,
                            offset: Offset(0, 10),
                            blurRadius: 10),
                      ]),
                  child: new ListView.builder(
                    itemCount: _users.length,
                    itemBuilder: _buildUsersListTile,
                  ),
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: ElevatedButton(
                      onPressed: (){
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Done',
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      ),
                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blue),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.blue)
                          )
                      )),
                    )
                ),
                Positioned(
                  left: 20,
                  right: 20,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 45,
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(45)),
                        child: Image.asset("assets/images/friends.png")),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}