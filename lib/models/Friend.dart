import 'dart:convert';
import 'package:meta/meta.dart';

class Friend {
  Friend({
    //@required this.avatar,
    @required this.id,
    @required this.name,
    @required this.email,
    @required this.country,
  });

  //final String avatar;
  final String id;
  final String name;
  final String email;
  final String country;

  factory Friend.fromJson(Map<String, dynamic> json) => Friend(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    country: json["country"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "country": country
  };
}